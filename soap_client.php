<?php
$options = Array(
    'uri' => 'http://localhost/',
    'location' => 'http://localhost/PHPCamp/soap_server.php',
    'trace' => 1
);
$client = new SoapClient(NULL, $options);
var_dump($_GET);
if(isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'checkProduct':
            echo '<pre>';
            print_r($client->checkProduct($_GET['id']));
            echo '</pre>';
            break;
        case 'addProduct':
            if (isset($_GET['name']) && isset($_GET['price'])) {
                $name = $_GET['name'];
                $price = $_GET['price'];
                if ($name != '' && (float) $price > 0) {
                    $client->addProduct($name, $price);
                }
            }
            break;
        case 'removeProduct':
            if (isset($_GET['id'])) {
                $id = (int)$_GET['id'];
                if ($id > 0) {
                    $client->removeProduct($id);
                }
            }
            break;
    }
}