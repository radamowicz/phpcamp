<?php

/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 30.05.2017
 * Time: 09:10
 */
class Product
{
    private $id;
    private $name;
    private $price;

    /**
     * Product constructor.
     * @param $id
     * @param $name
     * @param $price
     */
    public function __construct($id, $name, $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }

}
