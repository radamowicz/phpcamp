<?php
require_once 'database/databaseConfig.php';
require_once 'database/DB.php';
class ServiceFunctions
{

    private $db;

    public function __construct()
    {
        $this->db = new DB(DATABASE_HOST, DATABASE_USER,
            DATABASE_PASSWORD, 'phpcamp');
    }

    private function getTable() {
        $sql = "SELECT id, name, price
                FROM products";
        $this->db->query($sql);
        return $this->db->getAllRows();
    }

    public function checkProduct($id)
    {
        $sql = "SELECT id, name, price
                FROM products
                WHERE id = $id";
        $this->db->query($sql);
        return $this->db->getRow();
    }

    public function addProduct($name, $price) {
        $sql = "INSERT INTO products(`name`, `price`)
                            VALUES ('$name', $price);";
        $this->db->query($sql);
    }

    public function removeProduct($id) {
        $sql = "DELETE FROM products
                WHERE id = $id;";
        $this->db->query($sql);
    }
}
$db = new DB(DATABASE_HOST, DATABASE_USER,
    DATABASE_PASSWORD, 'phpcamp');
$options = Array('uri' => 'http://localhost');
$server = new SoapServer(NULL, $options);
$server->setClass('ServiceFunctions');
$server->handle();