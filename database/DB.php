<?php
require 'DB.interface.php';

/**
 * Created by PhpStorm.
 * User: Radek
 * Date: 29.05.2017
 * Time: 15:30
 */
class DB implements iDB
{
    private $link;
    private $result;

    public function __construct($host, $login, $password, $dbName)
    {
        $this->link = mysqli_connect($host, $login, $password);
        mysqli_select_db($this->link, $dbName);
        if (!$this->link) {
            throw new Exception ('Nie udalo sie polaczyc z baza danych: ' . $dbName);
        }
    }

    public function query($query)
    {
        $this->result = mysqli_query($this->link, $query);
        return (bool)$this->result;
    }

    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->link);
    }

    public function getRow()
    {
        return mysqli_fetch_assoc($this->result);
    }

    public function getAllRows()
    {
        $arr = Array();
        while ($row = mysqli_fetch_assoc($this->result)) {
            $arr[] = $row;
        }
        return $arr;
    }

}