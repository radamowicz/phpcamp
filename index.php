<?php
require_once 'database/databaseConfig.php';
require_once 'database/DB.php';
//require_once 'classes/Product.php';
$db = new DB(DATABASE_HOST, DATABASE_USER,
            DATABASE_PASSWORD, 'phpcamp');
$sql = '';
if(isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'checkProduct':
            if (isset($_GET['id'])) {
                $id = (int) $_GET['id'];
                if ($id > 0) {
                    $sql = "SELECT name
                        FROM products
                        WHERE id = $id
                        LIMIT 1;";
                    $db->query($sql);
                    echo $db->getRow()['name'];
                }
            }
            break;
        case 'addProduct':
            if (isset($_GET['name']) && isset($_GET['price'])) {
                $name = $_GET['name'];
                $price = $_GET['price'];
                if ($name != '' && (float) $price > 0) {
                    $sql = "INSERT INTO products(`name`, `price`)
                            VALUES ('$name', $price);";
                    $db->query($sql);
                }
            }
            break;
        case 'removeProduct':
            if (isset($_GET['id'])) {
                $id = (int) isset($_GET['id']);
                if ($id > 0) {
                    $sql = "DELETE FROM products
                            WHERE id = $id;";
                    $db->query($sql);
                }
            }
            break;
    }
}

function xml_from_indexed_array($array, $parent, $name) {
    echo '<?xml version="1.0"?>', "\n";
    echo "<$parent>\n";
    foreach ($array as $element) {
        echo "  <$name>\n";
        foreach ($element as $child => $value) {
            echo "    <$child>$value</$child>\n";
        }
        echo "  </$name>\n";
    }
    echo "</$parent>\n";
}

$sql = "SELECT *
        FROM products";
$db->query($sql);
xml_from_indexed_array($db->getAllRows(), 'products', 'product');